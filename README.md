# High Availability WP Cloudformation Template

To use this template with AWS CLI, you first have to create an IAM user with Administrative access and ensure it has adequate hardening.

You install AWS CLI on your machine, follow the [installation guide](https://aws.amazon.com/cli/).

On Mac, you can use Brew:

```
brew install awscli
```

With the IAM role properly set up, you can then proceed to running:

```
aws configure
```

For the sake of sanity and not to see errors, I will recommend using the link to this file on Gitlab or you upload the file to S3 
because the main template file is really huge.

## First get the Zone set up:

Run this in the directory you clone this repo to, remember the parameters files live in this directory.

```
aws cloudformation create-stack --stack-name WPZone --template-url https://prhomhyse.s3-us-west-2.amazonaws.com/zones.yml  --parameters file://zone-parameters.json
```

I used the S3 link in this case.

Also take note the `--stack-name`, in this case I named it `WPZone`

If you call it something else, please update it when needed. 

Once the zone is done.

Update the DNS record of the domain to be served from **Route 53**, this will prevent the Certs or Cloudfront from having any issues.


## Get the VPC running

```
aws cloudformation create-stack --stack-name haWPVPC --template-url https://prhomhyse.s3-us-west-2.amazonaws.com/vpc-2az.yml
```

Again, you can reference the file from repo.

## The Main Template

Before you run this template:

Create certificates for ELB and Cloudfront. It is recommended to use a wildcard certificate. For instance I created a ***.example.com** certificate.

Create the first certificate for ELB in the **`us-west-2`** region and the second certificate for Cloudfront **MUST** be in the **`us-east-1`**. 

When the certificates are issued, you'd see the ARN field that looks similar to this:

```
ARN	arn:aws:acm:us-west-2:731016540330:certificate/e87a2953-057b-4d24-90fa-5ce31d91e8b3
```
Note:

`ha-parameter.json` is the parameter file that will be used for the main High Availability WordPress set up. 

You can update the values as needed.

NOTE: For this parameter `WebServerKeyName` not to cause the CloudFormation stack provisioning to fail, enter an existing EC2 key you already have. 

Run the script:

```
aws cloudformation create-stack --stack-name HAWP --template-url https://prhomhyse.s3-us-west-2.amazonaws.com/aws-cfn.yml  --parameters file://parameters.json --capabilities CAPABILITY_IAM
```

The stack created depends on the VPC and Zone stacks you created earlier. 